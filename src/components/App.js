import React, {Component} from 'react'
import PlansPage from './main/plan/PlansPage'
import {Route, Switch, withRouter} from 'react-router-dom'
import Plan from './plan/Plan'
import '../styles/App.css'
import MainPageContent from './main/MainPageContent'
import Grid from '@material-ui/core/Grid'
import PlanBar from './main/PlanBar'
import planAPI from '../api/planAPI'
import '../styles/main/user/SignUpDialog.css'
import '../styles/main/user/LoginDialog.css'
import AddPlan from './plan/form/AddPlan'
import UserSettings from './main/user/form/UserSettings'
import UserProfile from './main/user/UserProfile'
import SuccessSnackbar from './common/SuccessSnackbar'
import 'react-dates/lib/css/_datepicker.css'
import SearchedPlansPage from './main/plan/SearchedPlansPage'
import EditPlan from './plan/form/EditPlan'
import PlansShelf from './plan/PlansShelf'
import FavePlans from './plan/FavePlans'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            currentUser: {},
            isAuthenticated: false,
            success: {
                open: false,
                message: ''
            },
        }
    }

    componentDidMount() {
        if (localStorage.getItem('accessToken')) {
            this.loadUser()
        }
    }

    loadUser = () => {
        planAPI.get('api/user/me')
            .then(response => {
                this.setState({
                    currentUser: response,
                    isAuthenticated: true
                })
            })
            .catch(error => {
                console.log(error)
                return null
            })
    }

    handleLoginSubmit = (e) => {
        event.preventDefault()
        const form = e.target
        const data = new FormData(form)

        for (let name of data.keys()) {
            data.set(name, data.get(name))
        }

        planAPI.post('api/auth/signin',
            data,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        )
            .then(response => {
                    localStorage.setItem('accessToken', response.data.accessToken)
                    this.loadUser()
                }
            )
            .catch(error => {
                console.log(error)
                return null
            })

    }

    handleSignUpSubmit = (e) => {
        event.preventDefault()
        const form = e.target
        const data = new FormData(form)

        for (let name of data.keys()) {
            data.set(name, data.get(name))
        }
        console.log('data ', ...data)

        planAPI.post('api/auth/signup',
            data,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        )
            .then(response => {
                //TODO: DISPLAY TOASTER WITH MESSAGE
                this.setState({
                    ...this.state,

                })
                history.go(0)
                }
            )
            .catch(error => {
                console.log(error)
                return null
            })

    }

    handleLogout = () => {
        localStorage.removeItem('accessToken')

        this.setState({
            currentUser: {},
            isAuthenticated: false
        })

        this.props.history.push('/main')
    }

    handleClose = () => {
        console.log('close')
    }

    render() {
        console.log(this.state)
        return <Grid container>
            <Grid item xs={12}>
                <PlanBar
                    currentUser={this.state.currentUser}
                    isAuthenticated={this.state.isAuthenticated}
                    handleLogout={this.handleLogout}
                    handleLoginSubmit={this.handleLoginSubmit}
                    handleSignUpSubmit={this.handleSignUpSubmit}
                />

            </Grid>
            <Route exact path="/main"
                   render={(props) => <MainPageContent currentUser={this.state.currentUser} {...props}  />}/>
            <Route exact path="/plans"
                   render={(props) => <PlansPage currentUser={this.state.currentUser}
                                                 path={'/plans'}
                                                 {...props} />}/>
            <Route exact path="/plans/search"
                   render={(props) => <SearchedPlansPage currentUser={this.state.currentUser}
                                                         {...props} />}/>

            <Route exact path="/plans/faves"
                   render={(props) => <FavePlans currentUser={this.state.currentUser}
                                                 {...props}  />}/>
            {this.state.currentUser.data && <Route exact path="/plans/faved"
                                                   render={(props) => <PlansPage currentUser={this.state.currentUser}
                                                                                 path={`plans/faved?by=${this.state.currentUser.data.username}`}
                                                                                 {...props}  />}/>}

            <Route exact path="/plans/shelf"
                   render={(props) => <PlansShelf currentUser={this.state.currentUser} {...props}  />}/>
            <Route exact path="/plans/plan"
                   render={(props) => <Plan currentUser={this.state.currentUser} {...props}  />}/>
            <Route exact path="/plans/add"
                   render={(props) => <AddPlan
                                               currentUser={this.state.currentUser} {...props}  />}/>
            <Route exact path="/plans/edit"
                   render={(props) => <EditPlan
                       currentUser={this.state.currentUser} {...props}  />}/>
            <Switch> <Route exact path="/user/settings"
                            render={(props) => <UserSettings currentUser={this.state.currentUser}
                                                             {...props} />}/>
                <Route exact path="/user/:username"
                       render={(props) => <UserProfile currentUser={this.state.currentUser}
                                                       {...props} />}/>
            </Switch>

            {this.state.success.open ?
                <SuccessSnackbar open={this.state.success.open}
                                 message={this.state.success.message}
                                 handleClose={this.handleClose}/>
                : null}

            {/*<Grid item xs={12}> <PlanFooter/></Grid>*/}
        </Grid>

    }
}

export default withRouter(App)
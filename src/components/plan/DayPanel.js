import React, {Component} from 'react'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import {AddCircle, RemoveCircle} from '@material-ui/icons'
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton'
import '../../styles/plan/DayPanel.css'
import SelectAutoSuggest from '../common/SelectAutoSuggest'
import PlanAutoSuggest from '../common/PlanAutoSuggest'
import {last} from 'lodash'

class DayPanel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            location: '',
            rows: [
                {
                    title: '',
                    description: ''
                }]
        }
    }


    handleAddRow = () => {
        const item = {
            title: '',
            description: ''
        }
        this.setState({
            rows: [...this.state.rows, item]
        })
    }

    handleRemoveSpecificRow = idx => () => {
        this.setState({
            rows: this.state.rows.filter((s, sidx) => idx !== sidx)
        })
    }

    handleChange = idx => evt => {
        const newRows = this.state.rows.map((row, sidx) => {
            if (idx !== sidx) {
                return row
            }
            return {...row, [evt.target.name.split('.')[2]]: evt.target.value}
        })

        this.setState({rows: newRows})
    }

    componentDidMount() {
        this.setState({
            ...this.state,
            location: this.props.location || this.state.location,
            rows: this.props.dailyPlan || this.state.rows
        })
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.location !== this.props.location) {
            this.setState({
                ...this.state,
                location: this.props.location
            })
        }
    }

    render() {
        console.log('DayPanel props ', this.props)
        // console.log('DayPanel state ', this.state)

        return <Grid item xs={12}>

            {this.props.isEditForm ?
                <PlanAutoSuggest
                    isEditForm={this.props.isEditForm}
                    planType={this.props.planType}
                    name={`dailyPlan[${this.props.day}].location`}
                    location={this.state.location}
                    onChange={this.props.onChange}
                />
                :
                <SelectAutoSuggest
                    planType={this.props.planType}
                    name={`dailyPlan[${this.props.day}].location`}
                    location={this.state.location}
                    onChange={this.props.onChange}
                />
            }

            {this.state.rows.map((row, i) => (
                <React.Fragment key={i}> <Grid item xs={12} className={'content-row'}>
                    <TextField
                        className={'panel-input'}
                        label={'Title'}
                        margin="normal"
                        variant="outlined"
                        name={`dailyPlan[${this.props.day}].places[${i}].title`}
                        value={row.title}
                        onChange={this.handleChange(i)}
                    />
                    <Tooltip title="Remove row"><IconButton disabled={this.state.rows.length <= 1}
                                                            onClick={this.handleRemoveSpecificRow(i)}>
                        <RemoveCircle className={this.state.rows.length <= 1 && 'remove-row'}/>
                    </IconButton></Tooltip>
                </Grid>
                    <Grid item xs={12} className={'content-row'}>
                        <TextField
                            className={'panel-input'}
                            label={'Description'}
                            margin="normal"
                            variant="outlined"
                            multiline
                            rows={4}
                            rowsMax={6}
                            name={`dailyPlan[${this.props.day}].places[${i}].description`}
                            value={row.description}
                            onChange={this.handleChange(i)}
                        />
                        {last(this.state.rows) === row &&
                        <Tooltip title="Add row"><IconButton onClick={this.handleAddRow}>
                            <AddCircle/>
                        </IconButton></Tooltip>}
                    </Grid>
                </React.Fragment>
            ))}

        </Grid>

    }
}

export default DayPanel
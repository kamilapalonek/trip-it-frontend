import React, {Component} from 'react'
import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import {BeachAccess, Delete, Edit} from '@material-ui/icons'
import {withRouter} from 'react-router-dom'
import Container from '@material-ui/core/Container'
import '../../styles/plan/PlansShelf.css'
import AddFolderDialog from './AddFolderDialog'

class PlansShelf extends Component {

    render() {
        return <Container className={'shelf-container'}>
            <Grid item xs={12} md={6} className={'grid-item'}>
                <AddFolderDialog/>

            </Grid>

            <Grid item xs={12} md={6} className={'grid-item'}>
                <div>
                    <List>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar className={'folder-icon'}>
                                    <BeachAccess/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText
                                primary="Italy"
                                secondary={'Nov 9, 2019'}
                            />
                            <ListItemSecondaryAction>
                                <IconButton edge="end" aria-label="edit">
                                    <Edit/>
                                </IconButton>
                                <IconButton edge="end" aria-label="delete">
                                    <Delete/>
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    </List>
                </div>
            </Grid>
        </Container>
    }
}

export default withRouter(PlansShelf)
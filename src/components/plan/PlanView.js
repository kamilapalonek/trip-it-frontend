import React from 'react'
import Container from '@material-ui/core/Container'
import DayByDay from './DayByDay'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Avatar from '@material-ui/core/Avatar'
import {AttachMoney, DateRange, FavoriteBorder, Flight, LocationOn} from '@material-ui/icons'
import CardMedia from '@material-ui/core/CardMedia'
import '../../utils/apiUtils'
import '../../styles/plan/PlanView.css'
import moment from 'moment'
import {Link} from 'react-router-dom'

const PlanView = ({plan}) => {
    const dailyPlan = plan.dailyPlan
    const planInfo = plan.planInfo || {}

    return <div id={'toPDF'}>
        <CoverPhotoWithContent {...plan}/>
        <Grid item xs={12}>
            <Container className={'container'}>
                <Grid item xs={6}> <AuthorCard {...plan}/></Grid>
                <Grid item xs={12}> <DayByDay {...dailyPlan}/></Grid>
                <Grid container justify="center" spacing={3}>
                    <GridElement title={'Must-Have'} content={planInfo.mustHave}/>
                    <GridElement title={'Must-Skip'} content={planInfo.mustSkip}/>
                    <GridElement title={'Tips & Comments'} content={planInfo.otherComments}/>
                </Grid>
            </Container>
        </Grid>
    </div>
}

const CoverPhotoWithContent = ({title, dailyPlan, daysSpent, faves, coverPhoto}) => {
    return <React.Fragment>
        <CardMedia
            className={'cover-photo'}
            image={coverPhoto || 'https://source.unsplash.com/featured/?venice'}
            component={'img'}/>
        <div className={'image-content'}>
            <h1>{title}</h1>
            <p>{dailyPlan && dailyPlan[0].location} - {daysSpent} days</p>
            <div className={'favorite-content'}><FavoriteBorder/>
                <Typography>{faves || '0'}</Typography></div>
        </div>
    </React.Fragment>
}

const getMonthFromDate = (stringDate) => {
    const date = moment(stringDate, 'MM/dd/yyyy').toDate()
    return moment(date).format('MMMM')
}

const AuthorCard = ({author, dailyPlan, startDate, daysSpent, costs}) => {
    console.log('startDate ', startDate)

    const month = startDate ? getMonthFromDate(startDate) : ''

    return <Card className={'user-card'}>
        <CardContent>
            <div className={'user-info'}>
                <Avatar src={author && author.avatar || 'https://source.unsplash.com/featured/?girl'}/>
                <Link to={`/user/${author ? author.username : ''}`}><Typography variant="h5"
                                                                                component="h2">{author ? author.name : ''}</Typography></Link>
            </div>
            <Typography variant="body2" color="textSecondary">
                <Typography><LocationOn style={{fontSize: 20}}/> {dailyPlan && dailyPlan[0].location}</Typography>
                <Typography><Flight style={{fontSize: 20}}/> {month}</Typography>
                <Typography><DateRange style={{fontSize: 20}}/> {daysSpent}</Typography>
                <Typography><AttachMoney style={{fontSize: 20}}/> {costs}</Typography>
            </Typography>
        </CardContent>
    </Card>
}

const GridElement = ({title, content}) => {
    return <Grid item xs={3}>
        <Paper elevation={0}>
            <Typography variant="h6">
                {title}
            </Typography>
            <Typography className={'grid-content'}>
                {content || 'Author left no comment'}
            </Typography>
        </Paper>
    </Grid>
}

export default PlanView
import React, {Component} from 'react'
import PlanView from './PlanView'
import '../../utils/apiUtils'
import planAPI from '../../api/planAPI'

export default class Plan extends Component {
    state = {
        plan: {}
    }

    componentDidMount() {
        this.getPlan()
    }

    getPlan = () => {
        const location = this.props.location
        const url = location.pathname + location.search

        planAPI
            .get(url)
            .then(data => this.setState({plan: data.data}))
            .catch(error => {
                console.log(error)
                return null
            })
    }

    render() {
        return <PlanView {...this.state}/>
    }
}
import React, {Component} from 'react'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import DayDetails from './DayDetails'
import TabPanel from './TabPanel'
import '../../styles/plan/DayByDay.css'

export default class DayByDay extends Component {
    state = {
        value: 0
    }

    handleChange = (e, next) => {
        this.setState({value: next})
    }

    render() {
        const props = this.props
        const value = this.state.value
        const places = Object.values(props)
        return <div className={'day-tabs'}>
            <AppBar position="static">
                {this.getHeaders(value, props)}
            </AppBar>
            {this.getContent(value, props, places)}
        </div>

    }

    getContent = (value, props, places) => {
        return <React.Fragment>{Object.keys(props).map((val, i) =>
            <TabPanel value={value} index={i} key={i}>
                <DayDetails places={places[i]}/>
            </TabPanel>
        )}</React.Fragment>
    }

    getHeaders = (value, props) => {
        console.log(value)
        console.log(props)
        return <Tabs value={value} variant="scrollable" onChange={this.handleChange}>
            {Object.keys(props).map(
                (val, index) => <Tab label={`Day ${parseInt(val) + 1}`} index={index} key={index}/>)}
        </Tabs>
    }
}

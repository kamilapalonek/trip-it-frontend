import React, {Component} from 'react'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import '../../../styles/main/plan/AddPlan.css'
import TextField from '@material-ui/core/TextField'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'
import {DateRangePicker} from 'react-dates'
import Button from '@material-ui/core/Button'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import {times} from 'lodash'
import Tab from '@material-ui/core/Tab'
import TabPanel from '../TabPanel'
import DayPanel from '../DayPanel'
import planAPI from '../../../api/planAPI'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Radio from '@material-ui/core/Radio'
import PlanAutoSuggest from '../../common/PlanAutoSuggest'

class AddPlan extends Component {

    state = {
        value: 0,
        planType: 'city',
        location: ''
    }

    handleChange = (e, next) => {
        this.setState({value: next})
    }


    handleOnChange = (e) => {
        const target = e.target
        console.log(target.value)
        this.setState({
            ...this.state,
            [target.name]: target.value,
            location: ''
        })

    }

    handlePlanChange = (i, e) => {
        const {name, value} = e.target

    }


    handleFormSubmit = (e) => {

        event.preventDefault()
        const form = e.target
        const data = new FormData(form)
        const userId = this.props.currentUser.data && this.props.currentUser.data.id || ''

        planAPI.post(`/plans/plan?userId=${userId}`,
            data,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        )
            .then(response => {
                    console.log(response)
                this.props.history.push('/main')
                }
            )
            .catch(error => {
                console.log(error)
                return null
            })
    }

    getHeaders = (value, days) => {
        return <Tabs value={value} variant="scrollable" onChange={this.handleChange}>
            {times(days, i => <Tab label={`Day ${parseInt(i) + 1}`} index={i} key={i}/>)}
        </Tabs>
    }

    getContent = (value, days, planType) => {
        return <React.Fragment>{times(days, i => <TabPanel value={value} index={i} key={i}>
            <DayPanel day={i}
                      onDataChange={this.handlePlanChange}
                      rowNumber={value}
                      dailyPlan={this.state.dailyPlan}
                      planType={planType}
                      location={this.state.location}
                      onChange={this.onChange}
            />
        </TabPanel>)}</React.Fragment>

    }

    onChange = (e) => {
        console.log('ONCHANGE', e.suggestion)
        this.setState({
            ...this.state,
            location: e.suggestion && e.suggestion.value || e.value
        })
    }

    render() {
        console.log('PLAN STATE ', this.state)
        console.log('PLAN PROPS ', this.props)

        const days = this.state && this.state.endDate ? this.state.endDate.diff(this.state.startDate, 'days') : 0
        const value = this.state.value
        return <React.Fragment>
            <Grid item xs={12}>
                <Container className={'container'}>
                    <form noValidate autoComplete="off"
                          onSubmit={this.handleFormSubmit}>
                        <Grid item xs={6}>
                            <RadioGroup name="planType"
                                        value={this.state.planType}
                                        onChange={this.handleOnChange}>
                                <FormControlLabel value="city" control={<Radio/>} label="City"/>
                                <FormControlLabel value="country" control={<Radio/>} label="Multi-city"/>
                            </RadioGroup>

                            {this.state.planType === 'city' &&
                            <PlanAutoSuggest
                                location={this.state.location}
                                onChange={this.onChange}/>}

                        </Grid>
                        <Grid item xs={6}> <TextField
                            id="outlined-name-input"
                            className={'plan-info'}
                            label="Title"
                            name="title"
                            margin="normal"
                            variant="outlined"
                            required
                        /> </Grid>
                        <Grid item xs={6}> <TextField
                            id="outlined-name-input"
                            className={'plan-info'}
                            label="Cover photo link"
                            name="coverPhoto"
                            margin="normal"
                            variant="outlined"
                            required
                        /> </Grid>

                        <Grid item xs={12}>
                            <DateRangePicker
                                startDate={this.state.startDate}
                                startDateId="startDate"
                                endDate={this.state.endDate}
                                endDateId="endDate"
                                onDatesChange={({startDate, endDate}) => this.setState({
                                    startDate,
                                    endDate
                                })}
                                focusedInput={this.state.focusedInput}
                                onFocusChange={focusedInput => this.setState({focusedInput})}
                                hideKeyboardShortcutsPanel
                            />

                            {days > 0 && <div><AppBar position="static">
                                {this.getHeaders(value, days)}
                            </AppBar>
                                {this.getContent(value, days, this.state.planType)}
                            </div>
                            }
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                id="outlined-name-input"
                                label="Costs"
                                name="costs"
                                margin="normal"
                                variant="outlined"
                                required
                            /> </Grid>

                        <Grid item xs={12}>
                            <TextField
                                id="outlined-multiline-static"
                                className={'plan-info'}
                                label="Must-have"
                                multiline
                                rows="4"
                                margin="normal"
                                variant="outlined"
                                name="planInfo.mustHave"
                            /></Grid>

                        <Grid item xs={12}>
                            <TextField
                                id="outlined-multiline-static"
                                className={'plan-info'}
                                label="Must-skip"
                                multiline
                                rows="4"
                                margin="normal"
                                variant="outlined"
                                name="planInfo.mustSkip"
                            /></Grid>
                        <Grid item xs={12}>
                            <TextField
                                id="outlined-multiline-static"
                                className={'plan-info'}
                                label="Tips & comments"
                                multiline
                                rows="4"
                                margin="normal"
                                variant="outlined"
                                name="planInfo.otherComments"
                            /></Grid>
                        <Button type="submit" color="primary">
                            Submit
                        </Button>
                    </form>

                </Container>
            </Grid>
        </React.Fragment>
    }
}

export default AddPlan
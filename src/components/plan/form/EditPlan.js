import React, {Component} from 'react'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import '../../../styles/main/plan/AddPlan.css'
import TextField from '@material-ui/core/TextField'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'
import {DateRangePicker} from 'react-dates'
import Button from '@material-ui/core/Button'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import {isEmpty, times} from 'lodash'
import Tab from '@material-ui/core/Tab'
import TabPanel from '../TabPanel'
import DayPanel from '../DayPanel'
import planAPI from '../../../api/planAPI'
import {withRouter} from 'react-router-dom'
import moment from 'moment'
import SuccessSnackbar from '../../common/SuccessSnackbar'
import PlanAutoSuggest from '../../common/PlanAutoSuggest'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Radio from '@material-ui/core/Radio'

class EditPlan extends Component {

    state = {
        success: {
            open: false,
            message: ''
        },
        plan: {
            dailyPlan: []
        },
        value: 0
    }

    handleChange = (e, next) => {
        this.setState({value: next})
    }


    handleOnChange = (e) => {
        const target = e.target

        this.setState({
            ...this.state,
            plan: {
                ...this.state.plan,
                [target.name]: target.value
            }

        })
    }

    handlePlanInfoChange = (e) => {
        const target = e.target
        const name = target.name.split('.')[1]
        console.log(target.name)
        console.log(target.value)
        this.setState({
            plan: {
                ...this.state.plan,
                planInfo: {
                    ...this.state.plan.planInfo,
                    [name]: target.value
                }
            }
        })

    }

    handlePlanChange = (i, e) => {
        const {name, value} = e.target

    }

    handleClose = () => {
        this.setState({
            success: {
                open: false
            }
        })
    }

    onLocationChange = (e) => {
        console.log('ONCHANGE', e.suggestion)
        this.setState({
            ...this.state,
            location: e.suggestion && e.suggestion.value || e.value
        })
    }

    componentDidMount() {
        planAPI.get('/plans/plan' + location.search)
            .then(response => this.setState({
                plan: response.data
            }))
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.plan.dailyPlan !== prevState.plan.dailyPlan) {
            this.setState({
                ...this.state,
                plan: {
                    ...this.state.plan,
                    dailyPlan: this.state.plan.dailyPlan
                },
                location: this.state.plan.dailyPlan[0].location
            })
        }
        if (this.state.plan.planType !== prevState.plan.planType) {
            this.setState({
                ...this.state,
                plan: {
                    ...this.state.plan,
                    planType: this.state.plan.planType
                }
            })
        }
    }


    handleFormSubmit = (e) => {

        event.preventDefault()
        console.log(e.target)
        const form = e.target
        let data = new FormData(form)
        const userId = this.props.currentUser.data.id
        for (let name of data.keys()) {
            data.set(name, data.get(name))
        }

        if (!isEmpty(this.state.plan.id)) {
            data.set('id', this.state.plan.id)
        }

        if (!isEmpty(this.state.plan.planType)) {
            data.set('planType', this.state.plan.planType)
        }



        planAPI.post(`/plans/plan?userId=${userId}`,
            data,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        )
            .then(response => {
                    console.log(response)
                this.setState(
                    {
                        success: {
                            open: true,
                            message: response.data
                        }
                    })
                }
            )
            .catch(error => {
                console.log(error)
                return null
            })
    }

    getHeaders = (value, days) => {
        return <Tabs value={value} variant="scrollable" onChange={this.handleChange}>
            {times(days, i => <Tab label={`Day ${parseInt(i) + 1}`} index={i} key={i}/>)}
        </Tabs>
    }

    getContent = (value, days, planType) => {
        return <React.Fragment>{times(days, i => <TabPanel value={value} index={i} key={i}>
            <DayPanel day={i}
                      onDataChange={this.handlePlanChange}
                      rowNumber={value}
                      dailyPlan={this.state.plan.dailyPlan[i].places}
                      planType={planType}
                      location={this.state.plan.dailyPlan[i].location}
                      isEditForm={true}
            />
        </TabPanel>)}</React.Fragment>

    }

    getDays = () => {
        if (this.state.plan) {
            const endDate = moment(this.state.plan.endDate, 'MM-DD-YYYY')
            const startDate = moment(this.state.plan.startDate, 'MM-DD-YYYY')
            return endDate.diff(startDate, 'days')
        }
        return 0
    }


//TODO: wait on update about date range picker from material-ui :)
    render() {
        console.log('EDIT PLAN STATE ', this.state)
        console.log('EDIT PLAN PROPS ', this.props)

        const days = this.getDays()
        const {value, plan} = this.state
        return <React.Fragment>
            <Grid item xs={12}>
                <Container className={'container'}>
                    <form noValidate autoComplete="off" onSubmit={this.handleFormSubmit}>
                        <Grid item xs={6}>
                            <RadioGroup
                                name="planType"
                                value={plan.planType}>
                                <FormControlLabel value="city"
                                                  control={<Radio/>}
                                                  label="City"
                                                  checked={this.state.plan.planType === 'city'}
                                                  disabled
                                />
                                <FormControlLabel value="country"
                                                  control={<Radio/>}
                                                  label="Multi-city"
                                                  checked={this.state.plan.planType === 'country'}
                                                  disabled
                                />

                            </RadioGroup>
                            {plan.planType === 'city' &&
                            <PlanAutoSuggest
                                isEditForm={true}
                                planType={plan.planType}
                                location={this.state.location}
                                onChange={this.onLocationChange}
                            />}

                        </Grid>
                        <Grid item xs={6}> <TextField
                            id="outlined-name-input"
                            className={'plan-info'}
                            label="Title"
                            name="title"
                            margin="normal"
                            variant="outlined"
                            value={plan.title || ''}
                            onChange={this.handleOnChange}
                            required
                        /> </Grid>
                        <Grid item xs={6}> <TextField
                            id="outlined-name-input"
                            className={'plan-info'}
                            label="Cover photo link"
                            name="coverPhoto"
                            margin="normal"
                            variant="outlined"
                            value={plan.coverPhoto || ''}
                            onChange={this.handleOnChange}
                            required
                        /> </Grid>

                        <Grid item xs={12}>
                            <DateRangePicker
                                startDate={plan.startDate && moment(plan.startDate, 'MM-DD-YYYY')}
                                startDateId="startDate"
                                endDate={plan.endDate && moment(plan.endDate, 'MM-DD-YYYY')}
                                endDateId="endDate"
                                onDatesChange={({startDate, endDate}) => this.setState({
                                    ...this.state,
                                    plan: {
                                        ...this.state.plan,
                                        startDate,
                                        endDate
                                    }
                                })}
                                focusedInput={this.state.focusedInput}
                                onFocusChange={focusedInput => this.setState({focusedInput})}
                                hideKeyboardShortcutsPanel
                            />

                            {days > 0 && <div><AppBar position="static">
                                {this.getHeaders(value, days)}
                            </AppBar>
                                {this.getContent(value, days, plan.planType)}
                            </div>
                            }
                        </Grid>

                        <Grid item xs={6}>
                            <TextField
                                id="outlined-name-input"
                                label="Costs"
                                name="costs"
                                margin="normal"
                                variant="outlined"
                                value={plan.costs || ''}
                                onChange={this.handleOnChange}
                                required
                            /> </Grid>

                        <Grid item xs={12}>
                            <TextField
                                id="outlined-multiline-static"
                                className={'plan-info'}
                                label="Must-have"
                                multiline
                                rows="4"
                                margin="normal"
                                variant="outlined"
                                name="planInfo.mustHave"
                                value={plan.planInfo && plan.planInfo.mustHave || ''}
                                onChange={this.handlePlanInfoChange}
                            /></Grid>

                        <Grid item xs={12}>
                            <TextField
                                id="outlined-multiline-static"
                                className={'plan-info'}
                                label="Must-skip"
                                multiline
                                rows="4"
                                margin="normal"
                                variant="outlined"
                                name="planInfo.mustSkip"
                                value={plan.planInfo && plan.planInfo.mustSkip || ''}
                                onChange={this.handlePlanInfoChange}
                            /></Grid>
                        <Grid item xs={12}>
                            <TextField
                                id="outlined-multiline-static"
                                className={'plan-info'}
                                label="Tips & comments"
                                multiline
                                rows="4"
                                margin="normal"
                                variant="outlined"
                                name="planInfo.otherComments"
                                value={plan.planInfo && plan.planInfo.otherComments || ''}
                                onChange={this.handlePlanInfoChange}
                            /></Grid>
                        <Button type="submit" color="primary">
                            Submit
                        </Button>
                    </form>
                    {this.state.success.open &&
                    <SuccessSnackbar open={this.state.success.open}
                                     message={this.state.success.message}
                                     handleClose={this.handleClose}/>}
                </Container>
            </Grid>
        </React.Fragment>
    }
}

export default withRouter(EditPlan)
import React from 'react'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import StepContent from '@material-ui/core/StepContent'
import Typography from '@material-ui/core/Typography'
import ShelfModal from './ShelfModal'

const DayDetails = (props) => {
    const {places} = props.places || {}

    return <Stepper orientation="vertical">
                {places && places.map(
                    (place, index) => <Step active={true} key={index}>
                        <StepLabel>{place.title}
                            <ShelfModal/>
                        </StepLabel>
                        <StepContent>
                            {place.description && <Typography variant={'body2'}>{place.description}</Typography>}
                        </StepContent>
                    </Step>
                )
                }
            </Stepper>

}

export default DayDetails
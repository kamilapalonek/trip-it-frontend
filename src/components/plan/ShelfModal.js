import React, {Component} from 'react'
import {Add} from '@material-ui/icons'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import {withRouter} from 'react-router-dom'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton'
import '../../styles/plan/ShelfModal.css'

class ShelfModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            folder: ''
        }
        this.exampleRef = React.createRef()
    }

    toggleOpen = () => {
        this.setState({open: !this.state.open})
    }

    handleChange = (e) => {
        console.log(e.target.value)
        const target = e.target
        this.setState({
            [target.name]: target.value
        })
    }

    render() {
        return <React.Fragment>
            <Tooltip title="Save to shelf"><IconButton size={'small'} onClick={this.toggleOpen}>
                <Add size={'small'}/>
            </IconButton></Tooltip>
            <Dialog className={'login-dialog'} open={this.state.open} onClose={this.toggleOpen}
                    aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Save to shelf</DialogTitle>
                <form className={'shelf-form'} noValidate autoComplete="off" onSubmit={this.props.handleLoginSubmit}>
                    <div>
                        <FormControl variant="outlined">
                            <InputLabel ref={this.exampleRef} id="demo-simple-select-outlined-label">
                                Age
                            </InputLabel>
                            <Select
                                labelId={'demo-simple-select-outlined-label'}
                                className={'folder-picker'}
                                variant="outlined"
                                autoWidth
                                value={this.state.folder}
                                onChange={this.handleChange}>
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value={'Venice'}>Venice</MenuItem>
                                <MenuItem value={'Italy'}>Italy</MenuItem>
                                <MenuItem value={'World'}>World</MenuItem>
                                <MenuItem value={'My super shelf'}>My super shelf</MenuItem>
                            </Select>
                        </FormControl></div>
                    <div>
                        <TextField
                            id="outlined-full-width"
                            label="Description"
                            name="planDescription"
                            margin="normal"
                            variant="outlined"
                            multiline={true}
                            rows={1}
                            rowsMax={2}
                        /></div>
                    <Button id={'submit-button'} type="submit" color="primary">
                        Submit
                    </Button>
                </form>
            </Dialog>
        </React.Fragment>
    }
}

export default withRouter(ShelfModal)
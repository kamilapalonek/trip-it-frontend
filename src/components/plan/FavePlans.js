import React from 'react'
import {withRouter} from 'react-router-dom'
import PlansPage from '../main/plan/PlansPage'

const FavePlans = (props) => {
    return <PlansPage currentUser={props.currentUser}
                      path={'/plans/faves'}
                      infoMessage={'You haven\'t added anything to your favorites yet.'}/>
}

export default withRouter(FavePlans)
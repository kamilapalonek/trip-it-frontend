import React, {Component} from 'react'
import {Add} from '@material-ui/icons'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import {withRouter} from 'react-router-dom'
import Typography from '@material-ui/core/Typography'

class AddFolderDialog extends Component {

    state = {
        open: false
    }

    toggleOpen = () => {
        this.setState({open: !this.state.open})
    }

    render() {
        return <React.Fragment>
            <Button variant="contained" onClick={this.toggleOpen}>
                <Typography className={'text-button'}>Add</Typography>
                <Add className={'add-icon'}/>
            </Button>

            <Dialog className={'login-dialog'} open={this.state.open} onClose={this.toggleOpen}
                    aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Add new folder</DialogTitle>
                <form noValidate autoComplete="off" onSubmit={this.props.handleLoginSubmit}>
                    <TextField
                        className={'text-field'}
                        id="outlined-name-input"
                        label="Folder name"
                        name="folderName"
                        margin="normal"
                        variant="outlined"
                    />
                    <Button id={'submit-button'} type="submit" color="primary">
                        Submit
                    </Button>
                </form>
            </Dialog>
        </React.Fragment>
    }
}

export default withRouter(AddFolderDialog)
import React, {Component} from 'react'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Slider from '@material-ui/core/Slider'
import Input from '@material-ui/core/Input'
import Button from '@material-ui/core/Button'
import {withRouter} from 'react-router-dom'
import {isEmpty} from 'lodash'
import SelectAutoSuggest from '../common/SelectAutoSuggest'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Radio from '@material-ui/core/Radio'

class SearchForm extends Component {
    state = {
        searchType: 'city',
        location: '',
        days: [1, 4],
        budget: [100, 1000]
    }

    handleOnChange = (e) => {
        const target = e.target
        this.setState({
            ...this.state,
            [target.name]: target.value,
        })
    }

    handleDayChange = (event, newValue) => {
        this.setState({days: newValue})
    }

    handleBudgetChange = (event, newValue) => {
        this.setState({budget: newValue})
    }

    handleInputChange = event => {
        this.setState({budget: event.target.value === '' ? '' : Number(event.target.value)})
    }

    handleFormSubmit = (e) => {
        event.preventDefault()

        const data = [...e.target.elements].reduce((data, element) => {
            if (element.name && element.value) {
                data[element.name] = element.value
            }
            return data
        }, {})

        this.props.history.push({
            pathname: '/plans/search',
            search: '?' + new URLSearchParams(data).toString()
        })
    }

    componentDidMount() {
        console.log('lol ', this.props.values)

        if (!isEmpty(this.props.values)) {

            const {location, days, budgetTo, budgetFrom} = this.props.values
            const daysArray = days.split(',')

            this.setState(
                {
                    ...this.state,
                    location,
                    days: daysArray,
                    budget: [budgetFrom, budgetTo]
                }
            )

        }
    }

    render() {
        console.log('STATEEEE ', this.state)
        return <Paper className={'search-bar'}>
            <form noValidate autoComplete="off" onSubmit={this.handleFormSubmit}>
                {/*{!isEmpty(this.props.values) &&*/}
                {/*<IconButton size={'small'} onClick={this.toggleOpen}>*/}
                {/*    <ArrowUpward size={'small'} />*/}
                {/*</IconButton>*/}
                {/*}*/}

                <Typography id="location-search" gutterBottom>Where now?</Typography>

                <RadioGroup name="searchType"
                            value={this.state.searchType}
                            onChange={this.handleOnChange}>
                    <FormControlLabel value="city" color="primary" control={<Radio/>} label="City"/>
                    <FormControlLabel value="country" color="primary" control={<Radio/>} label="Country"/>
                </RadioGroup>

                {this.state.searchType === 'city' && <SelectAutoSuggest placeholder={'Search by city...'}
                                                                        location={this.state.location}
                                                                        searchType={'city'}
                />
                }

                {this.state.searchType === 'country' && <SelectAutoSuggest placeholder={'Search by country...'}
                                                                           location={this.state.location}
                                                                           searchType={'country'}
                />
                }

                <Typography id="range-slider" gutterBottom>
                    How many days?
                </Typography>
                <Slider
                    value={this.state.days}
                    name={'days'}
                    onChange={this.handleDayChange}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    min={1}
                    marks
                    steps={1}
                    max={30}
                />

                <Typography id="input-slider" gutterBottom>
                    What's your budget?
                </Typography>
                <Grid container spacing={2} alignItems="center">
                    <Grid item>
                        <Input
                            value={this.state.budget[0]}
                            name={'budgetFrom'}
                            margin="dense"
                            onChange={this.handleInputChange}
                            inputProps={{
                                step: 100,
                                min: 100,
                                max: 30000,
                                type: 'number',
                                'aria-labelledby': 'input-slider',
                            }}
                        />
                    </Grid>
                    <Grid item xs>
                        <Slider
                            value={this.state.budget}
                            onChange={this.handleBudgetChange}
                            aria-labelledby="range-slider"
                            min={100}
                            steps={500}
                            max={30000}
                        />
                    </Grid>

                    <Grid item>
                        <Input
                            value={this.state.budget[1]}
                            name={'budgetTo'}
                            margin="dense"
                            onChange={this.handleInputChange}
                            inputProps={{
                                step: 100,
                                min: 1000,
                                max: 30000,
                                type: 'number',
                                'aria-labelledby': 'input-slider',
                            }}
                        />
                    </Grid>
                </Grid>

                <Button id={'submit-form-button'} type="submit" color="primary">
                    Search
                </Button>

            </form>
        </Paper>
    }
}

export default withRouter(SearchForm)
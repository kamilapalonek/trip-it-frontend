import React, {Component} from 'react'
import planAPI from '../../../api/planAPI'
import PlanCard from './PlanCard'
import Container from '@material-ui/core/Container'
import '../../../styles/main/plan/PlansPage.css'
import Grid from '@material-ui/core/Grid'
import {isEmpty} from 'lodash'
import Typography from '@material-ui/core/Typography'

export default class PlansPage extends Component {
    state = {
        allPlans: []
    }

    componentDidMount() {
        this.getPlans(this.props.path)
    }

    getPlans = (path) => {
        planAPI
            .get(path)
            .then(response => this.setState({allPlans: response.data})
            )
            .catch(error => {
                console.log(error)
                return null
            })
    }

    render() {
        const {allPlans} = this.state
        return <Container className={'container'}>
            <Grid item xs={12} sm={6} className={'grid-item'}>{!isEmpty(allPlans) ? allPlans.map(plan => <PlanCard
                    key={plan.id}
                    currentUser={this.props.currentUser}
                    {...plan}
                />)
                : <Typography>{this.props.infoMessage || 'No plans found'}</Typography>
            }</Grid>
        </Container>
    }
}
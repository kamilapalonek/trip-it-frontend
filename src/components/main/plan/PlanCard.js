import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import {Edit, Favorite, FavoriteBorder} from '@material-ui/icons'
import '../../../styles/main/plan/PlanCard.css'
import planAPI from '../../../api/planAPI'
import {isEmpty} from 'lodash'

class PlanCard extends Component {
    state = {
        favClicked: false,
    }

    getFavesIcon = () => this.state.favClicked ? <Favorite className={'favorite'}/> : <FavoriteBorder/>

    onIconButtonClick = () => {
        planAPI.post('plans/plan/faves', null, {
                params: {
                    username: this.props.currentUser.data.username,
                    planId: this.props.id
                }
            }
        ).then(response => {
                console.log('response ', response)
                this.setState({
                    favClicked: !this.state.favClicked,
                    faves: response.data
                })
            }
        )
            .catch(error => {
                console.log(error)
                return null
            })

    }

    componentDidMount() {
        if (!isEmpty(this.props.usersFaved)) {

            const a = this.props.usersFaved.reduce(
                v => v.username === this.props.currentUser.data.username
            )

            if (a) {
                this.setState({
                    faves: this.props.faves,
                    favClicked: true
                })
            }
        }

    }

    render() {
        console.log(this.props)
        console.log(this.state)
        const {author, dailyPlan, planType, faves, coverPhoto, currentUser, id} = this.props
        const authorUsername = author && author.username
        const currentUsername = currentUser.data && currentUser.data.username

        const url = `/plans/plan?id=${id}`

        return <Card className={'card'}>
            <Link className={'title'} to={url}>
                <CardActionArea>
                    <CardMedia className={'card-media'}
                               component='img'
                               image={coverPhoto || 'https://source.unsplash.com/featured/?venice'}
                               title={location}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {this.props.title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {author && author.name || ''} @
                            {planType === 'city' ? dailyPlan[0].location : dailyPlan[0].location.split(',')[2]}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Link>
            <CardActions>
                <IconButton onClick={this.onIconButtonClick}>
                    {this.state.favClicked ? <Favorite className={'favorite'}/> : <FavoriteBorder/>}
                </IconButton>{this.state.faves || '0'}
                {currentUsername === authorUsername &&
                <IconButton><Link to={`/plans/edit?id=${id}`}><Edit/></Link></IconButton>}
            </CardActions>
        </Card>

    }
}

export default PlanCard
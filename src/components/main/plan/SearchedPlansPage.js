import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import planAPI from '../../../api/planAPI'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import PlanCard from './PlanCard'
import '../../../styles/main/plan/PlansPage.css'
import '../../../styles/main/plan/SearchedPlansPage.css'
import SearchForm from '../SearchForm'
import {parse} from 'query-string'
import {isEmpty} from 'lodash'
import Typography from '@material-ui/core/Typography'

class SearchedPlansPage extends Component {

    state = {
        allPlans: [],
        value: 0
    }

    handleChange = (e, next) => {
        this.setState({value: next})
    }


    componentDidMount() {
        this.loadPlan()
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        return 'value from snapshot'
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.location.search !== this.props.location.search) {
            this.loadPlan()
        }
    }

    loadPlan = () => {
        const {location} = this.props.history
        planAPI.get(location.pathname + location.search)
            .then(response => {
                    console.log(response)
                    this.setState(
                        {
                            allPlans: response.data
                        })
                }
            )
            .catch(error => {
                console.log(error)
                return null
            })


    }


    render() {
        const {allPlans} = this.state
        const params = parse(this.props.location.search)
        return <Container className={'container search-plans'}>
            <Grid item xs={12} sm={6}>{!isEmpty(allPlans) ?
                allPlans.map(plan => <PlanCard key={plan.id}
                                               currentUser={this.props.currentUser}
                                               {...plan}/>)
                : <Typography>No plans found. Try another search.</Typography>}
            </Grid>
            <Grid item xs={12} sm={6}>
                <SearchForm values={params}/></Grid>
        </Container>
    }
}

export default withRouter(SearchedPlansPage)
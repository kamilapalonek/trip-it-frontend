import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import '../../styles/main/PlanBar.css'
import IconButton from '@material-ui/core/IconButton'
import {AddLocation, Bookmarks, Loyalty} from '@material-ui/icons'
import Tooltip from '@material-ui/core/Tooltip'
import '../../api/planAPI'
import AccountMenu from './user/AccountMenu'
import LoginDialog from './user/form/LoginDialog'
import SignUpDialog from './user/form/SignUpDialog'
import {Link} from 'react-router-dom'

const PlanBar = (props) => {
    const {currentUser, isAuthenticated, handleLogout, handleLoginSubmit, handleSignUpSubmit} = props

        return <AppBar className={'plan-bar'}>
            <Toolbar>
                <div className={'globe-icon'}>
                    <img src={'https://image.flaticon.com/icons/svg/48/48704.svg'} alt={'Globe Icon'}/>
                </div>
                <Typography className={'app-title'} variant="h6"><Link to={'/main'}>Wanderluster</Link></Typography>
                <div className={'app-icons'}>
                    {isAuthenticated ? <React.Fragment>
                            <AccountMenu handleLogout={handleLogout} username={currentUser.data.username}/>
                            <Tooltip title="Add new plan"><IconButton>
                                <Link to={'/plans/add'}><AddLocation/></Link>
                            </IconButton></Tooltip>
                            <Tooltip title="Favorite plans"><IconButton>
                                <Link to={`/plans/faved?by=${currentUser.data.username}`}><Loyalty/></Link>
                            </IconButton></Tooltip>
                            <Tooltip title="Shelf"><IconButton>
                                <Link to={`/plans/shelf`}><Bookmarks/></Link>
                            </IconButton></Tooltip>
                        </React.Fragment>
                        : <React.Fragment>
                            <LoginDialog handleLoginSubmit={handleLoginSubmit}/>
                            <SignUpDialog handleSignUpSubmit={handleSignUpSubmit}/>
                        </React.Fragment>
                    }
                </div>
            </Toolbar>
        </AppBar>
}

export default PlanBar
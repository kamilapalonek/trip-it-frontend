import React from 'react'
import Grid from '@material-ui/core/Grid'
import ExplorePage from './ExplorePage'
import '../../styles/main/MainPageContent.css'
import {withRouter} from 'react-router-dom'
import SearchForm from './SearchForm'

const MainPageContent = () => {
    return <React.Fragment>
        <Grid item xs={12} sm={6}><ExplorePage/></Grid>
        <Grid item xs={12} sm={6}><SearchForm/></Grid>
    </React.Fragment>
}

export default withRouter(MainPageContent)
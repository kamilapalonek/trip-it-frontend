import {AccountCircle} from '@material-ui/icons'
import React, {Component} from 'react'
import {Link, withRouter} from 'react-router-dom'
import IconButton from '@material-ui/core/IconButton'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

class AccountMenu extends Component {
    state = {
        anchorEl: null
    }

    handleClick = (event) => {
        this.setState({anchorEl: event.currentTarget})
    }

    handleClose = () => {
        this.setState({anchorEl: null})
    }

    render() {
        console.log('ACCOUNT MENU', this.props)
        return <React.Fragment>
            <IconButton onClick={this.handleClick}>
                <AccountCircle/>
            </IconButton>
            <Menu
                id="simple-menu"
                anchorEl={this.state.anchorEl}
                open={Boolean(this.state.anchorEl)}
                onClose={this.handleClose}
            >
                <MenuItem component={Link} to={`/user/${this.props.username}`}>Profile</MenuItem>
                <MenuItem component={Link} to="/user/settings">Settings</MenuItem>
                <MenuItem onClick={this.props.handleLogout}>Logout</MenuItem>
            </Menu>

        </React.Fragment>
    }
}

export default withRouter(AccountMenu)
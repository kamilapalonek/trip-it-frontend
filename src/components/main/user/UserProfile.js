import React, {Component} from 'react'
import planAPI from '../../../api/planAPI'
import {withRouter} from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import CardContent from '@material-ui/core/CardContent'
import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import {Cake, Room} from '@material-ui/icons'
import moment from 'moment'
import '../../../styles/main/user/UserProfile.css'
import PlanCard from '../plan/PlanCard'
import {isEmpty} from 'lodash'

class UserProfile extends Component {

    state = {
        user: {},
        plans: []
    }

    componentDidMount() {
        const {username} = this.props.match.params
        this.loadUser(username)
        this.loadUserPlans(username)
    }

    loadUser = (username) => {
        planAPI.get(`/api/user/${username}`)
            .then(response => {
                console.log('HERE', response.data)
                this.setState({
                    user: response.data
                })
            })
            .catch(error => {
                console.log(error)
                return null
            })
    }

    loadUserPlans = (username) => {
        console.log('PLANS')
        planAPI.get(`/plans/user?username=${username}`)
            .then(response => {
                console.log('response ', response.data)
                this.setState({
                    plans: response.data
                })
            })
            .catch(error => {
                console.log(error)
                return null
            })
    }

    getAge = (stringDate) => {
        const date = moment(stringDate, 'YYYY-MM-DD').toDate()
        return date ? moment().diff(date, 'years') : ''
    }


    render() {
        const {user, plans} = this.state
        console.log(user)

        let age = user.userInfo ? this.getAge(user.userInfo.birthDate) : ''

        return <React.Fragment>
            <Grid item xs={12}>
                <Container className={'container'}>
                    <Grid item xs={6} className={'user-container'}>
                        <Card className={'profile-card'}>
                            <CardContent>
                                <div className={'user-info'}>
                                    <Avatar src={user.avatar || 'https://source.unsplash.com/featured/?woman'}/>
                                    <Typography variant="h5"
                                                component="h2">{user.name || ''}</Typography>
                                    <Typography color="textSecondary">@{user.username}</Typography>
                                </div>
                                {user.userInfo && <React.Fragment>
                                    <Typography variant="body2" color="textSecondary">
                                        {user.userInfo.description || ''}</Typography>
                                    {user.userInfo.location ?
                                        <div className={'profile-info'}>
                                            <Room/><Typography>{user.userInfo.location}</Typography></div>
                                        : null}
                                    {age ? <div className={'profile-info'}><Cake/><Typography>{age}</Typography>
                                    </div> : null}
                                </React.Fragment>
                                }
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography className={'profile-title'} variant="h6">Plans by {user.name}</Typography>
                        {!isEmpty(plans) ?
                            <Grid item xs={12}>
                                <div className={'plan-grid'}>
                                    {plans.map(plan => <PlanCard key={plan.id}
                                                                 currentUser={this.props.currentUser}
                                                                 {...plan}/>)}
                                </div>
                            </Grid>
                            : <Typography className={'profile-title'}>User has not posted any plans</Typography>
                        }
                    </Grid>
                </Container>
            </Grid>
        </React.Fragment>
    }
}

export default withRouter(UserProfile)
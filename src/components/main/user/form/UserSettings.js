import React, {Component} from 'react'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import {isEmpty} from 'lodash'
import planAPI from '../../../../api/planAPI'
import MomentUtils from '@date-io/moment'
import {KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers'
import '../../../../styles/main/user/UserSettings.css'
import moment from 'moment'
import SuccessSnackbar from '../../../common/SuccessSnackbar'
import SelectAutoSuggest from '../../../common/SelectAutoSuggest'

class UserSettings extends Component {
    state = {
        user: {},
        success: {
            open: false,
            message: ''
        },
        failure: {
            open: false,
            message: ''
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        return 'value from snapshot'
    }

    componentDidMount() {
        if (!isEmpty(this.props.currentUser.data)) {
            this.loadUserDetails(this.props.currentUser.data.id)
        }
    }

    componentDidUpdate(prevProps, prevState, snapshotValue) {
        if (prevProps.currentUser.data !== this.props.currentUser.data) {
            this.loadUserDetails(this.props.currentUser.data.id)
        }
    }

    loadUserDetails = (id) => {
        planAPI.get(`/api/user?id=${id}`)
            .then(response => this.setState({
                user: response.data
            }))
            .catch(error => {
                console.log(error)
                return null
            })
    }

    handleValueChange = (e) => {
        const target = e.target

        this.setState({
            user: {
                ...this.state.user,
                [target.name]: target.value
            }
        })

    }

    handleChange = (e) => {
        const target = e.target
        const name = target.name.split('.')[1]

        this.setState({
            user: {
                ...this.state.user,
                userInfo: {
                    ...this.state.user.userInfo,
                    [name]: target.value
                }
            }
        })
    }

    handleDateChange = (date) => {
        const formattedDate = date.format('YYYY-MM-DD')
        this.setState({
            user: {
                ...this.state.user,
                userInfo: {
                    ...this.state.user.userInfo,
                    birthDate: formattedDate
                }
            }
        })
    }

    handleFormSubmit = (e) => {
        event.preventDefault()
        const form = e.target
        const data = new FormData(form)

        for (let name of data.keys()) {
            data.set(name, data.get(name))
        }
        console.log(...data)
        planAPI.post(`/api/user?id=${this.props.currentUser.data.id}`,
            data,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        )
            .then(response => {
                    console.log(response)
                this.setState(
                    {
                        success: {
                            open: true,
                            message: response.data
                        }
                    })
                }
            )
            .catch(error => {
                console.log(error)
                return null
            })
    }

    handleClose = () => {
        this.setState({
            success: {
                open: false
            }
        })
    }

    render() {
        const currentDate = moment()

        return <React.Fragment>
            <Grid item xs={12}>
                <Container className={'container'}>
                    <Grid item xs={6} className={'user-container'}>
                        <form noValidate autoComplete="off" onSubmit={this.handleFormSubmit}>
                            <TextField
                                className={'text-field'}
                                id="outlined-avatar-input"
                                label="Avatar link"
                                name="avatar"
                                margin="normal"
                                variant="outlined"
                                value={this.state.user.avatar || ''}
                                onChange={this.handleValueChange}
                            />
                            <TextField
                                className={'text-field'}
                                id="outlined-name-input"
                                label="Name"
                                name="name"
                                margin="normal"
                                variant="outlined"
                                value={this.state.user.name || ''}
                                onChange={this.handleValueChange}
                            />
                            <TextField
                                disabled
                                className={'text-field'}
                                id="outlined-username-input"
                                helperText="Username cannot be changed"
                                label="Username"
                                name="username"
                                margin="normal"
                                variant="outlined"
                                value={this.state.user.username || ''}
                            />
                            <TextField
                                className={'text-field'}
                                id="outlined-email-input"
                                label="Email"
                                type="email"
                                name="email"
                                margin="normal"
                                variant="outlined"
                                value={this.state.user.email || ''}
                                onChange={this.handleValueChange}
                            />
                            <TextField
                                className={'text-field'}
                                id="outlined-password-input"
                                label="Password"
                                type="password"
                                name="password"
                                margin="normal"
                                variant="outlined"
                                onChange={this.handleValueChange}
                            />
                            <MuiPickersUtilsProvider utils={MomentUtils}>
                                <KeyboardDatePicker
                                    className={'date-picker'}
                                    margin="normal"
                                    id="date-picker-dialog"
                                    name="userInfo.birthDate"
                                    label="Birth date"
                                    format="YYYY-MM-DD"
                                    value={this.state.user.userInfo && this.state.user.userInfo.birthDate || currentDate}
                                    onChange={this.handleDateChange}/>
                            </MuiPickersUtilsProvider>
                            <SelectAutoSuggest placeholder={'Search...'}
                                               name={'userInfo.location'}
                                               location={this.state.user.userInfo && this.state.user.userInfo.location || ''}
                                               type={'city'}
                                               onChange={this.handleChange}
                            />
                            <TextField
                                id="outlined-full-width"
                                label="Description"
                                name="userInfo.description"
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                multiline={true}
                                rows={1}
                                rowsMax={4}
                                value={this.state.user.userInfo && this.state.user.userInfo.description || ''}
                                onChange={this.handleChange}/>
                            <Button id={'submit-form-button'} type="submit" color="primary">
                                Submit
                            </Button>
                        </form>
                    </Grid>

                    {this.state.success.open &&
                        <SuccessSnackbar open={this.state.success.open}
                                         message={this.state.success.message}
                                         handleClose={this.handleClose}/>}

                </Container>
            </Grid>
        </React.Fragment>
    }
}

export default UserSettings
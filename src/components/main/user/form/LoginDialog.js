import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import '../../../../styles/main/user/LoginDialog.css'
import {Person} from '@material-ui/icons'
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton'


class LoginDialog extends Component {

    state = {
        open: false
    }

    toggleOpen = () => {
        this.setState({open: !this.state.open})
    }

    render() {
        return <React.Fragment>
            <Tooltip title="Sign In"><IconButton onClick={this.toggleOpen}>
                <Person/>
            </IconButton></Tooltip>

            <Dialog className={'login-dialog'} open={this.state.open} onClose={this.toggleOpen}
                    aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Log In</DialogTitle>
                <form noValidate autoComplete="off" onSubmit={this.props.handleLoginSubmit}>
                    <TextField
                        className={'text-field'}
                        id="outlined-email-input"
                        label="Username or email"
                        type="email"
                        name="usernameOrEmail"
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        className={'text-field'}
                        id="outlined-password-input"
                        label="Password"
                        type="password"
                        name="password"
                        margin="normal"
                        variant="outlined"
                    />
                    <Button id={'submit-button'} type="submit" color="primary">
                        Submit
                    </Button>
                </form>
            </Dialog>
        </React.Fragment>
    }
}

export default withRouter(LoginDialog)
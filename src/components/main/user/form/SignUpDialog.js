import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import '../../../../styles/main/user/SignUpDialog.css'
import IconButton from '@material-ui/core/IconButton'
import {PersonAdd} from '@material-ui/icons'
import Tooltip from '@material-ui/core/Tooltip'

class SignUpDialog extends Component {

    state = {
        open: false
    }

    toggleOpen = () => {
        this.setState({open: !this.state.open})
    }

    render() {
        return <React.Fragment>
            <Tooltip title="Sign Up"><IconButton onClick={this.toggleOpen}>
                <PersonAdd/>

            </IconButton></Tooltip>
            <Dialog className={'sign-up-dialog'} open={this.state.open} onClose={this.toggleOpen}
                    aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Sign Up</DialogTitle>
                <form noValidate autoComplete="off" onSubmit={this.props.handleSignUpSubmit}>
                    <TextField
                        className={'text-field'}
                        id="outlined-name-input"
                        label="Name"
                        name="name"
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        className={'text-field'}
                        id="outlined-username-input"
                        label="Username"
                        name="username"
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        className={'text-field'}
                        id="outlined-email-input"
                        label="Email"
                        type="email"
                        name="email"
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        className={'text-field'}
                        id="outlined-password-input"
                        label="Password"
                        type="password"
                        name="password"
                        margin="normal"
                        variant="outlined"
                    />
                    <Button id={'submit-button'} type="submit" color="primary">
                        Submit
                    </Button>
                </form>
            </Dialog>
        </React.Fragment>
    }
}

export default withRouter(SignUpDialog)
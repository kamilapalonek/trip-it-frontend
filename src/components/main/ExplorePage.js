import React from 'react'
import CardMedia from '@material-ui/core/CardMedia'
import '../../styles/main/ExplorePage.css'
import {ExploreOutlined, StarBorder, TrendingUpOutlined} from '@material-ui/icons'
import Typography from '@material-ui/core/Typography'
import {Link} from 'react-router-dom'

const icons = {
    explore: ExploreOutlined,
    star: StarBorder,
    new: TrendingUpOutlined
}

// TODO: new mapping & whole logic to displaying 10 new plans (i.e. 10 top plans this month)
// TODO: discover - view all plans based on some creationDate or sth

const ExplorePage = () => {
    return <div className={'explore-view'}>
        <PhotoWithContent
            imageUrl={'https://images.unsplash.com/photo-1521216774850-01bc1c5fe0da?ixlib=rb-1.2.1&auto=format&fit=crop&w=1500&q=80'}
            title={'Discover'}
            icon={icons.explore}
            path={'/plans'}/>

        <PhotoWithContent
            imageUrl={'https://images.unsplash.com/photo-1517035811173-b7659b8d0ddb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80'}
            title={'Favorite destinations of all time'}
            icon={icons.star}
            path={'/plans/faves'}
        />
        <PhotoWithContent
            imageUrl={'https://images.unsplash.com/photo-1475754073691-4423e1368422?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80'}
            title={'New inspirations'}
            icon={icons.new}
            path={'/plans/newest'}
        />

    </div>
}

const PhotoWithContent = (props) => {
    const CustomIcon = props.icon

    return <div className={'wrapper'}>
        <Link to={props.path}><CardMedia
            component={'img'}
            className={'explore-image'}
            image={props.imageUrl}/>
        <div id={'img-id'} className={'image-content'}>
            <CustomIcon/>
            <Typography> {props.title} </Typography>
        </div>
        </Link>
    </div>
}

export default ExplorePage
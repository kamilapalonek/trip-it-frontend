import React, {Component} from 'react'
import AlgoliaPlaces from './AlgoliaPlaces'

class SelectAutoSuggest extends Component {
    state = {
        location: ''
    }

    onChange = (e) => {
        console.log('ONCHANGE', e.suggestion)
        this.setState({
            ...this.state,
            location: e.suggestion && e.suggestion.value || e.value
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.location !== this.props.location) {
            this.setState(
                {
                    location: this.props.location
                }
            )
        }
    }

    render() {
        // console.log('SelectAutoSuggest props ', this.props)
        // console.log('SelectAutoSuggest state ', this.state)
        return <AlgoliaPlaces
            planType={this.props.planType}
            name={this.props.name || 'location'}
            placeholder={this.props.placeholder || 'Where?'}
            value={this.state.location}
            onChange={this.onChange}
            options={{
                appId: 'plCMGSZJLQUX',
                apiKey: '3d144a03b9b1e37aa735a66dc482139b',
                type: this.props.searchType || 'city',
                language: 'EN'
            }}/>
    }
}

export default SelectAutoSuggest
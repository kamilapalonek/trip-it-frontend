import React from 'react'
import {CheckCircle, Close} from '@material-ui/icons'
import Snackbar from '@material-ui/core/Snackbar'
import IconButton from '@material-ui/core/IconButton'
import '../../styles/common/SuccessSnackbar.css'

const SuccessSnackbar = (props) => {
    return <Snackbar
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
        }}
        variant={'success'}
        open={props.open}
        autoHideDuration={6000}
        onClose={props.handleClose}
        message={<span id="message-id"><CheckCircle/> {props.message}</span>}
        action={[
            <IconButton
                id={'snackbar'}
                key="close"
                aria-label="close"
                onClick={props.handleClose}>
                <Close className={'close-icon'}/>
            </IconButton>
        ]}
    />
}

export default SuccessSnackbar



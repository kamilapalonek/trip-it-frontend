import React from 'react'
import AlgoliaPlaces from './AlgoliaPlaces'

const PlanAutoSuggest = (props) => {
    return <AlgoliaPlaces
        isEditForm={props.isEditForm}
        planType={props.planType || 'city'}
        name={props.name || 'location'}
        placeholder={props.placeholder || 'Where?'}
        value={props.location}
        onChange={props.onChange}
        options={{
            appId: 'plCMGSZJLQUX',
            apiKey: '3d144a03b9b1e37aa735a66dc482139b',
            type: 'city',
            language: 'EN'
        }}/>
}

export default PlanAutoSuggest
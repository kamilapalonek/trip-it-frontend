import axios from 'axios'
import {BASE_URL} from '../utils/apiUtils'

const planAPI = axios.create({
    baseURL: BASE_URL,
    headers: {
        'Accept': 'application/json',
    }
    }
)

planAPI.interceptors.request.use(
    config => {
        let token = localStorage.getItem('accessToken')
        config.headers.Authorization = token ? `Bearer ${token}` : ''
        console.log(config)
        return config
    },
    error => Promise.reject(error)
)

export default planAPI
## Wanderluster

*Wanderluster* is an application that allows you to **add a travel plan** and its modifications. 
In addition, it allows users to **view plans and filter results** using a search engine. Additional functionalities, 
such as the **Favorites tab** or **Shelf tab**, combine the functionality of competing applications on the market, but present 
them in a different form. 

The application architecture is based on the **MVC architectural model and the client-server model**. 
The server part was written in **Java**, using modules from the **Spring framework**. The client part has been implemented in **JavaScript**, 
together with the **ReactJS library** and the user interface framework, **Material-UI**.

### Main view
![ImgName](https://bitbucket.org/kamilapalonek/trip-it-frontend/raw/master/images/main.png)
*Wanderluster* offers you to browse suggested itineraries (sections "Discover", "Favorite destinations of all time", "New inspirations") 
or search for plans via the search engine.
The application does not require logging in to be able to see the home page and perform a search.
However, in order to perform further actions, i.e. adding or displaying a plan, it is necessary to create an account and log in.

### Search view
![ImgName](https://bitbucket.org/kamilapalonek/trip-it-frontend/raw/master/images/search.png)
The system displays items matching the search parameters or a message about the lack of plans in the database. 
From this view, the **system allows you to modify queries and updates the results**. 
Additionally, a drop-down list is provided with options for sorting the results. By default, **plans are sorted with the most recent**.

### Plan view
![ImgName](https://bitbucket.org/kamilapalonek/trip-it-frontend/raw/master/images/plan-1.png)
![ImgName](https://bitbucket.org/kamilapalonek/trip-it-frontend/raw/master/images/plan-2.png)
![ImgName](https://bitbucket.org/kamilapalonek/trip-it-frontend/raw/master/images/plan-3.png)
The plan view consists of several sections. The first section is a summary of the plan that consists of information appearing on the background of the photo.
Practical information is shown under the user's name and profile photo, e.g. the month in which the author traveled or budget, which they allocated for the trip.
The last section includes a description of the trip and comments on the plan.
The application presents **what the author of the plan did or where they were, on individual days of departure and provides an optional description of each place**.
Below you will find information from the author about places worth visiting and avoiding, as well as additional comments
e.g. transport or overnight.

### Shelf view

![ImgName](https://bitbucket.org/kamilapalonek/trip-it-frontend/raw/master/images/shelf-1.png)
![ImgName](https://bitbucket.org/kamilapalonek/trip-it-frontend/raw/master/images/shelf-2.png)
*Wanderluster* offers the user the ability to **save places of interest from any plans**, using the Shelf tab. Shelf is a collection of user-created folders. Folders can have any name and store any number of places.

### Architecture
![ImgName](https://bitbucket.org/kamilapalonek/trip-it-frontend/raw/master/images/architecture.png)
The architecture of the application is based on the **MVC architectural pattern and the client-server model**. 
The server part was written in the **Java**, using modules from the **Spring framework**. 
The client part has been implemented in **JavaScript**, along with the **ReactJS library** and the **Material-UI framework**.